<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
*    
     */
    public function index (Request $request)
        {
            
            $tasks = Task::where('name_task', 'like', "%$request->search%")->orWhere('created_at', 'like', "%$request->search%")->orWhere('description_task', 'like', "%$request->search%")->orWhere('state', 'like', "%$request->search%")->sortable()->paginate(10);
        
        return view('home',compact('tasks'));
    }
}
