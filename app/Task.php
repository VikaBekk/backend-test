<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Task extends Model
{
    use Sortable;
    //
    protected $table = "tasks";
    protected $fillable = ['name_task', 'created_at', 'description_task','state'];
    public $sortable = [ 'name_task', 'created_at', 'description_task','state'];
    
}
