<?php

use Illuminate\Database\Seeder;
use App\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $state = ['new', 'in progress', 'complete'];
        for ($i = 0; $i <= 100; $i++) {

            $email = $faker->unique()->freeEmail;
            Task::create([
                'name_task' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'created_at' => $faker->dateTime(),
                'description_task' => $faker->text(),
                'state' => $state[array_rand($state)],
            ]);
        }
    }
}
