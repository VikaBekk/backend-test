@extends('layouts.log')

@section('content')
<!-- main -->
<div class="main-w3layouts wrapper">
	<h1>SignUp Form</h1>
	<div class="main-agileinfo">
		<div class="agileits-top">
			<form method="POST" action="{{ route('register') }}">
				@csrf
				<div>
					<input class="text form-group{{ $errors->has('name') ? ' has-error' : '' }}" type="text" id="name" name="name" placeholder="Username" required="">
					@if ($errors->has('name'))
					<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif
				</div>
				<div>
					<input class="text email form-group{{ $errors->has('email') ? ' has-error' : '' }}" " type="email" id="email" name="email" placeholder="Email" required="">
					@if ($errors->has('email'))
					<span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
				</div>
				<div>
					<input class="text form-group{{ $errors->has('password') ? ' has-error' : '' }}" type="password" id="password" name="password" placeholder="Password" required="">
					@if ($errors->has('password'))
					<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
				</div>
				<input class="text w3lpass" type="password" id="password-confirm" name="password_confirmation" placeholder="Confirm Password" required="">
				<div class="wthree-text">
					<label class="anim">
						<input type="checkbox" class="checkbox" required="">
						<span>I Agree To The Terms & Conditions</span>
					</label>
					<div class="clear"> </div>
				</div>
				<button type="submit" class="btn btn-primary">
					{{ __('Register') }}
				</button>
			</form>
			<p>Have an Account? <a href="{{ route('login') }}"> Login Now!</a></p>
		</div>
	</div>

	<!-- //copyright -->
	<ul class="colorlib-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
<!-- //main -->
@endsection