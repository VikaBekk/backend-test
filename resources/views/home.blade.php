@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">Tasks</h3>
                        </div>
                        <div class="col-md-6">
                            <div class="card-tools float-right">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="search" name="table_search" class="form-control float-right" onkeydown="if (event.keyCode == 13) filterData('search')" placeholder="Search">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th width="80px">@sortablelink('name_task')</th>
                                <th>@sortablelink('created_at')</th>
                                <th>@sortablelink('description_task')</th>
                                <th>@sortablelink('state')</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tasks as $task)
                            <tr>
                                <td> {{$task->name_task}}</a></td>
                                <td>{{$task->created_at}}</td>
                                <td>{{$task->description_task}}</td>
                                <td>{{$task->state}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="paginate-div">
                <a tabindex="" class="paginate-btn previous round {{request()->page <= 1 ? 'disabled' : ''}}" @if(request()->page <= 1) disabled @else onclick="filterData(-1)" @endif>&#8249;</a>
                <a tabindex="" class="paginate-btn next round {{request()->page >= $tasks->lastPage() ? 'disabled' : ''}}" @if(request()->page >= $tasks->lastPage()) disabled @else onclick="filterData(1)" @endif>&#8250;</a>
            </div>
        </div>
    </div>

</div>
@endsection