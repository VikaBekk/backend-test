<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Backend from Bekk</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
  <style>
    .paginate-btn {
      text-decoration: none;
      display: inline-block;
      padding: 8px 16px;
      cursor: pointer;
    }
    .paginate-div {
      margin: 30px 0;
      text-align: center;
    }

    .paginate-btn:hover {
      background-color: #ddd;
      color: black;
    }

    .previous {
      background-color: #4CAF50;
      color: white;
    }

    .next {
      background-color: #4CAF50;
      color: white;
    }

    .disabled {
      background-color: #f1f1f1;
      color: black;
      cursor: not-allowed;
    }

    .round {
      border-radius: 50%;
    }
    body {
    background: #76b852;
    /* fallback for old browsers */
    background: -webkit-linear-gradient(to top, #76b852, #8DC26F);
    background: -moz-linear-gradient(to top, #76b852, #8DC26F);
    background: -o-linear-gradient(to top, #76b852, #8DC26F);
    background: linear-gradient(to top, #76b852, #8DC26F);
    background-size: cover;
    background-attachment: fixed;
    font-family: 'Roboto', sans-serif;
  }
  </style>
</head>
<body>
    <div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Backend from Bekk
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script>
    const path = '/{{request()->path()}}'; // the current url
    let page = parseInt('{{request()->page}}') || 1; // the current page
    let search = ''; // search request

    /**
     * Search or get the new page
     *
     * @param mode
     * @return void
     */
    function filterData(mode) {
        search = document.getElementById('search').value;
        if (mode != 'search') page += mode;
        else page = 1;

        window.location.href = path + '?page=' + page + (search ? ('&search=' + search) : '');
    }
</script>
</body>
</html>
